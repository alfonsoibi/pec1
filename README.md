**PEC 1 - Un juego de aventuras**

Instrucciones del juego:

1. Pulsamos el botón "Jugar" para empezar la partida. Si pulsamos la cruz en la parte superior derecha, salimos del juego.
2. El turno será aleatorio, si al empezar la partida el contrincante nos insulta, tendremos que seleccionar la respuesta del "bocadillo" de nuestro jugador. Si es nuestro turno, elegimos el insulto que queramos del "bocadillo" de nuestro jugador.
3. Si seleccionamos la respuesta correcta, ganamos el turno y quitamos una vida al adversario, sino, el adversario nos quitará el turno y perderemos una vida.
4. Si insultamos al adversario y nos responde correctamente, nos quitará una vida y obtendrá el turno. Sino, le quitaremos una vida y mantendremos el turno.
5. El jugador que pierda las 3 vidas será el perdedor.

Aspectos generales:

- Los insultos y las respuestas se obtienen de dos txts ubicados en la carpeta resources. En uno tenemos los insultos y en el otro las respuestas. El primer insulto corresponde con la primera respuesta y así, sucesivamente.
- He creado un objeto "Frase" donde almacenar cada insulto y cada respuesta con un ID. Así sabremos que respuesta corresponde a cada insulto.
- Estos insultos y respuestas se guardan en un array de la clase "Frase" que posteriormente se desordena para mostrar los botones de forma aleatoria.
-El turno es aleatorio la primera vez, simpre obtendrá el turno el jugador que gane.
-Cuando el jugador elige un insulto, el pc recibe el ID, con lo que obtiene la respuesta correcta. Acertará el 50% de las veces. Hará un random entre 0 y 1, si sale 0, responderá correctamente, si sale 1, dirá ¡Ah si!, y perderá.
-Cuando insulta el PC, hará un Random entre todos los insultos y guardará el ID. Despúes, el jugador selecciona la respuesta y guarda el ID, si coincide, el jugador gana.
- Se han añadido elementos gráficos y sonidos al juego. Todos ellos están bajo licencia CC0.
- Los botones se instancian dentro de los "bocadillos" de los personajes para darle un toque más estético al juego.

ID del repositorio Gitlab: https://gitlab.com/alfonsoibi/pec1
Enlace al vídeo explicativo: https://youtu.be/uikPd_yBJ5c




