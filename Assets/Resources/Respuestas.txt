¿Por qué? ¿Acaso querías pedir uno prestado?
Sí que las hay, sólo que nunca las has aprendido.
Me alegra que asistieras a tu reunión familiar diaria.
Primero deberías dejar de usarla como un plumero.
Qué apropiado, tú peleas como una vaca.
Ya te están fastidiando otra vez las almorranas, ¿Eh?
Ah, ¿Ya has obtenido ese trabajo de barrendero?
Y yo tengo un SALUDO para ti, ¿Te enteras?
Te habrá enseñado todo lo que sabes.
¿TAN rápido corres?
Me haces pensar que alguien ya lo ha hecho.
Quería asegurarme de que estuvieras a gusto conmigo.
Qué pena me da que nadie haya oído hablar de ti
¿Incluso antes de que huelan tu aliento?
Estaría acabado si la usases alguna vez.
Espero que ya hayas aprendido a no tocarte la nariz.