﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pirataJugador : MonoBehaviour
{
    //Este script lo llamamos desde el "animationEvent" de la animación del personaje del jugador cuando muere.
    ControlEscena Controlador;
    public void Eliminado() {
        
        Controlador = FindObjectOfType<ControlEscena>();
        //Lanzamos la función FinPartida del script "ControlEscena"
        Controlador.FinPartida();
    }
}
