﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Versioning;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class ControlEscena : MonoBehaviour
{
    //Audios
    public AudioClip espada;
    public AudioClip cuchillo;

    //Objetos
    public GameObject[] vidasJugador;
    public GameObject[] vidasPC;
    public GameObject GuardarVencedor;
    public GameObject buttonPrefab;
    public GameObject listaDeBotonesInsultos;
    public GameObject listaDeBotonesRespuestas;
    public GameObject PanelInsultos;
    public GameObject PanelRespuestas;
    public GameObject PirataJugador;
    public GameObject PirataPC;
    public GameObject GloboPirataPc;

    //Animaciones
    public Animator animPirataJugador;
    public Animator animPirataPC;

    //Textos Insultos y respuestas

    TextAsset textoInsultos;
    TextAsset textoRespuestas;

    public Text PirataPCText;

    public int IDInsulto;
    public string Insulto;
    public int IDRespuesta;
    public string Respuesta;
    int idInsultoPirata;

    public int valorInsulto;
    public int valorRespuesta;

    string[] lineasInsultos;
    string[] lineasRespuestas;

    Frases[] listaInsultos;
    Frases[] listaRespuestas;

    //Otras variables
    int vencedor;
    public int turno;
    int vidaJugador;
    int vidaPC;

    void Start(){

        AudioSource audio = GetComponent<AudioSource>();
        vidaJugador = 3;
        vidaPC = 3;
        //Llamamos a la funcion recopilarDatos para obtener los textos de los insultos y las respuestas
        recopilarDatos();
        //Sorteamos qué jugador empieza
        establecerTurno();
    }

    public void recopilarDatos() {

        //Importamos los archivos de texto de la carpeta Resources dónde tenemos los insultos y las respuestas en dos txts separados.
        textoInsultos = (TextAsset)Resources.Load("Insultos");
        textoRespuestas = (TextAsset)Resources.Load("Respuestas");

        //Creamos un array para guardar cada una de las líneas de los documentos de texto.
        lineasInsultos = textoInsultos.text.Split('\n');
        lineasRespuestas = textoRespuestas.text.Split('\n');

        //Asignamos el tamaño que tendrá el array de la nueva clase
        listaInsultos = new Frases[lineasInsultos.Length];
        listaRespuestas = new Frases[lineasRespuestas.Length];

        //Rellenamos los arrays de la clase "Frases", de este modo, cada frase, tendrá un ID identificativo.
        for (int i = 0; i < lineasInsultos.Length; i++)
        {
            listaInsultos[i] = new Frases(lineasInsultos[i], i);
        }
        for (int i = 0; i < lineasRespuestas.Length; i++)
        {
            listaRespuestas[i] = new Frases(lineasRespuestas[i], i);
        }

        //Desordenamos los dos arrays
        Desordenar(listaInsultos, listaRespuestas);
        
        //Añadimos los valores a la clase Frase e instanciamos los botones.
        for (int i = 0; i < listaInsultos.Length; i++)
        {
            //Instanciamos los botones para los insultos
            GameObject botonInsulto = (GameObject)Instantiate(buttonPrefab, listaDeBotonesInsultos.transform);
            //Asignamos los dos valores de cada objeto
            botonInsulto.GetComponent<Frases>().F = listaInsultos[i].F;
            botonInsulto.GetComponent<Frases>().Id = listaInsultos[i].Id;
            //Asignamos el texto al boton
            botonInsulto.transform.GetChild(0).GetComponent<Text>().text = listaInsultos[i].F;
        }
        for (int i = 0; i < listaRespuestas.Length; i++)
        {
            //Instanciamos los botones para las respuestas
            GameObject botonRespuestas = (GameObject)Instantiate(buttonPrefab, listaDeBotonesRespuestas.transform);
            botonRespuestas.GetComponent<Frases>().F = listaRespuestas[i].F;
            botonRespuestas.GetComponent<Frases>().Id = listaRespuestas[i].Id;
            botonRespuestas.transform.GetChild(0).GetComponent<Text>().text = listaRespuestas[i].F;
        }
    }
    //Función que desordena los arrays
    public static void Desordenar<Frases>(IList<Frases> valuesIns, IList<Frases> valuesRes)
    {
        var n = valuesIns.Count;
        var rnd = new System.Random();
        for (int i = n - 1; i > 0; i--)
        {
            var j = rnd.Next(0, i + 1);
            var tempI = valuesIns[i];
            var tempR = valuesRes[i];
            valuesIns[i] = valuesIns[j];
            valuesRes[i] = valuesRes[j];
            valuesIns[j] = tempI;
            valuesRes[j] = tempR;
        }
    }

    public void establecerTurno() {
        //Hacemos un random para elegir el jugador que empieza la partida aleatoriamente
        int valorRandom = UnityEngine.Random.Range(0, 2);
        if (valorRandom == 0)
        {
            //Empieza el jugador
            turnoJugador();
            turno = 0;
        }
        else
        {
            //Empìeza el PC
            turnoPC();
            turno = 1;
        }  
    }

    public void turnoJugador()
    {
        //Activamos el panel para que el jugador seleccione un insulto.
        PanelInsultos.SetActive(true);  

    }
    public void turnoPC()
    {
        //Seleccionamos el isulto que lanzará el PC
        elegirInsultoPC();
        //Lanzamos la corrutina para que el PC realice el insulto.
        StartCoroutine(InsultarPC());
    }

    public void AccionesTurnoJugador(int id) {

        //Cuando se produce el OnClick del panel de Insultos del jugador, lanzamos esta funcion y seleccionamos la respuesta que corresponde al Id del insulto seleccionado.
        //Ocultamos el panel
        PanelInsultos.SetActive(false);
        //Lanzamos la funcion Elegir respuesta pasándo el Id de la respuesta correcta.
        ElegirRespuestaPC(id);
    }

    public void elegirInsultoPC()
    {
        //Elegimos un insulto al azar
        int rnd = UnityEngine.Random.Range(0, listaInsultos.Length);
        for (int i = 0; i < listaInsultos.Length; i++)
        {
            if (listaInsultos[i].Id == rnd)
            {
                //Guardamos el texto y el id del insulto
                PirataPCText.text = listaInsultos[i].F;
                idInsultoPirata = listaInsultos[i].Id;
            }
        }
    }

    public IEnumerator InsultarPC()
    {
        GloboPirataPc.SetActive(true);
        yield return new WaitForSeconds(3);
        GloboPirataPc.SetActive(false);
        //Activamos el panel de respuestas
        PanelRespuestas.SetActive(true);
    }

    public void AccionesTurnoPC(int id)
    {
        //Esta función la llamamos cuando hacemos click en uno de los botones de las respuestas del jugador
        //Comparamos el id que nos llega de la respuesta elegida con el id de la pregunta del PC
        if (id == idInsultoPirata)
        {
            //El jugador contesta correctamente
            StartCoroutine(GanaJugador());
        }
        else
        {
            //El jugador falla
            StartCoroutine(GanaPC());
        }
    }

    public void ElegirRespuestaPC(int id)
    {
        IDRespuesta = id;
        //Para que el PC no acierte siempre, hacemos que falle en un 50% de las ocasiones. 
        int rnd = UnityEngine.Random.Range(0, 2);
        //El PC acierta
        if (rnd == 0)
        {
            for (int i = 0; i < listaRespuestas.Length; i++)
            {
                if (listaRespuestas[i].Id == id)
                {
                    //Asignamos el texto de la respuesta
                    PirataPCText.text = listaRespuestas[i].F;
                    GloboPirataPc.SetActive(true);
                    //Lanzamos la corrutina GanaPC
                    StartCoroutine(GanaPC());
                }
            }
        }
        //El PC falla
        else
        {
            //Asignamos una respuesta absurda
            PirataPCText.text = "¡Ah si!";
            GloboPirataPc.SetActive(true);
            //Lanzamos la corrutina GanaJUgador
            StartCoroutine(GanaJugador());
        }
    }

    public IEnumerator GanaJugador()
    {
        //Restamos una vida al PC
        vidaPC = vidaPC -1;
        Destroy(vidasPC[vidaPC]);
        //Si el jugador tenía el turno
        if (turno == 0)
        {
            yield return new WaitForSeconds(2);
            GloboPirataPc.SetActive(false);
            GetComponent<AudioSource>().clip = espada;
            GetComponent<AudioSource>().Play();
            animPirataJugador.SetTrigger("attack");
            yield return new WaitForSeconds(1);
            turno = 0;
            //Si el PC se queda sin vidas muere
            if (vidaPC <= 0)
            {
                //Guardamos quien ha ganado para mostrarlo en la escena final
                vencedor = 0;
                //Lanzamos la animación de morir
                animPirataPC.SetTrigger("die"); 
            }
            else
            {
                //Asignamos el turno de nuevo al jugador
                turnoJugador();
            }
        }
        //Si el PC tenía el turno
        else
        {
            PanelRespuestas.SetActive(false);
            GetComponent<AudioSource>().clip = espada;
            GetComponent<AudioSource>().Play();
            animPirataJugador.SetTrigger("attack");
            yield return new WaitForSeconds(1);
            turno = 0;
            //Si el PC se queda sin vidas muere
            if (vidaPC <= 0)
            {
                //Guardamos quien ha ganado para mostrarlo en la escena final
                vencedor = 0;
                //Lanzamos la animación de morir
                animPirataPC.SetTrigger("die");        
            }
            else {
                //Asignamos el turno de nuevo al jugador
                turnoJugador();
            }       
        }
    }

    public IEnumerator GanaPC()
    {
        //Restamos una vida al Jugador
        vidaJugador = vidaJugador -1;
        Destroy(vidasJugador[vidaJugador]);
        //Si el jugador tenía el turno
        if (turno == 0)
        {
            yield return new WaitForSeconds(2);
            GloboPirataPc.SetActive(false);
            GetComponent<AudioSource>().clip = cuchillo;
            GetComponent<AudioSource>().Play();
            animPirataPC.SetTrigger("attack");
            yield return new WaitForSeconds(1);
            turno = 1;
            //Si el jugador se queda sin vidas muere
            if (vidaJugador <= 0)
            {
                //Guardamos quien ha ganado para mostrarlo en la escena final
                vencedor = 1;
                //Lanzamos la animación de morir
                animPirataJugador.SetTrigger("die");
            }
            else
            {
                //Asignamos el turno de nuevo al PC
                turnoPC();
            }
        }
        //Si el PC tenía el turno
        else
        {
            PanelRespuestas.SetActive(false);
            GetComponent<AudioSource>().clip = cuchillo;
            GetComponent<AudioSource>().Play();
            animPirataPC.SetTrigger("attack");
            yield return new WaitForSeconds(1);
            turno = 1;
            //Si el Jugador se queda sin vidas muere
            if (vidaJugador <= 0)
            {
                //Guardamos quien ha ganado para mostrarlo en la escena final
                vencedor = 1;
                //Lanzamos la animación de morir
                animPirataJugador.SetTrigger("die");
            }
            else
            {
                //Asignamos el turno de nuevo al PC
                turnoPC();
            }  
        }
    }

    public void FinPartida()
    {
        //Cuando se reproduce la animación de morir lanzamos un animationEvent que llamará a esta función. Aqui guardamos quien es el vencedor y cambiamos de escena.
        GuardarVencedor.GetComponent<GuardarVencedor>().Ganador = vencedor;
        SceneManager.LoadScene("Fin");
    }


    

    
    
    

}
