﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Frases : MonoBehaviour
{
    public string F;
    public int Id;
    ControlEscena Controlador;
    
    public Frases(string f, int id)
    {
        F = f;
        Id = id;
    }

    //Cuando creamos el botón, le pasamos la variable Id que guardaremos para comprobar si el la selección adecuada.
    public void crearButtonOnClick()
    {
        
        Controlador = FindObjectOfType<ControlEscena>();
        int turno = Controlador.turno;
        if (turno == 0)
        {
            Controlador.AccionesTurnoJugador(Id);
        }
        else {

            Controlador.AccionesTurnoPC(Id);
        }
       
        

    }

}
