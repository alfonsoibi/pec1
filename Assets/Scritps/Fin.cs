﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Fin : MonoBehaviour
{
    public GameObject pirataJugador;
    public GameObject pirataPC;
    public GameObject textoGanar;
    public GameObject textoPerder;
    public float fadeOut = 0.1f;
    public float volumen;
    public AudioClip sonidoGanar;
    public AudioClip sonidoPerder;
    AudioSource sonidoF;
    public int ganador;
    GameObject guardado;
    GameObject sonidoFondo;

    private void Start()
    {   
        sonidoFondo = GameObject.Find("SonidoFondo");
        sonidoF = sonidoFondo.GetComponent<AudioSource>();
        guardado = GameObject.Find("GuardarVencedor");
        ganador = guardado.GetComponent<GuardarVencedor>().Ganador;
        StartCoroutine(Final());  
    }

    public IEnumerator Final() {
        
        //Si el ganador es el jugador
        if (ganador == 0)
        {
            //Bajamos progresivamente el volumen del audio de fondo
            float startVolume = volumen;
            for (float i = 0; i < startVolume; i++)
            {
                sonidoF.volume--;
            }  
            sonidoF.Stop();
            //Asignamos y reproducimos el sonido del ganador
            GetComponent<AudioSource>().clip = sonidoGanar;
            GetComponent<AudioSource>().Play();
            //Mostramos que el jugador a ganado activando el personaje y el texto.
            pirataJugador.SetActive(true);
            textoGanar.SetActive(true);
        }
        //Si gana el PC
        else {
            float startVolume = volumen;
            for (float i = 0; i < startVolume; i++)
            {
                sonidoF.volume--;
            }
            sonidoF.Stop();
            GetComponent<AudioSource>().clip = sonidoPerder;
            GetComponent<AudioSource>().Play();
            pirataPC.SetActive(true);
            textoPerder.SetActive(true);
        }
        yield return new WaitForSeconds(3);
        //Eliminamos los objetos que manteniamos entre escenas
        Destroy(guardado);
        Destroy(sonidoFondo);
        //Volvemos a la escena inicial
        SceneManager.LoadScene("Menu");
    }
}
