﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardarVencedor : MonoBehaviour
{
    //Este escript lo utilizamos para almacenar el vencedor de la escena del juego para utilizarlo en la escena final.
    public int Ganador;

    void Awake() { 
        
        //Hacemos que este objeto no se destruya al cambiar de escena.
        DontDestroyOnLoad(this.gameObject); 
    }
    
    
    
}
