﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MantenerAudio : MonoBehaviour
{
    //Usamos este script para mantener el audio en las demás escenas.
    void Awake()
    {

        DontDestroyOnLoad(this.gameObject);
    }
}
