﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Menu : MonoBehaviour
{
    //Si pulsamos el botón jugar vamos a la escena del juego
    public void Jugar() {
        SceneManager.LoadScene("Juego");
    }

    //Si pulsamos el botón salir, cerramos el juego.
    public void Salir() {

        Application.Quit();
    }
}
